/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

Blockly.defineBlocksWithJsonArray([
  // Block for colour picker.
  {
    "type": "modelprovider",
    "message0": "ModelProvider %1 %2",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "input_value",
        "name": "Path",
        "check": "String"
      }
    ],
    "inputsInline": false,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "pathselect",
    "message0": "applicability %1 %2",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "input_statement",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "colour": 130,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "check",
    "message0": "check %1 %2",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "input_statement",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "colour": 130,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "typeselect",
    "message0": "Type %1 %2",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "input_value",
        "name": "typename"
      }
    ],
    "inputsInline": true,
    "previousStatement": null,
    "nextStatement": null,
    "colour": 240,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "typeattselect",
    "message0": "Type %1 %2 Attribute %3 %4",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "input_value",
        "name": "typename"
      },
      {
        "type": "input_dummy"
      },
      {
        "type": "input_value",
        "name": "attname"
      }
    ],
    "inputsInline": true,
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "checkingtask",
    "message0": "task %1 %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "input_statement",
        "name": "applicability",
        "align": "RIGHT"
      },
      {
        "type": "input_statement",
        "name": "check",
        "align": "RIGHT"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 10,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "predicate",
    "message0": "predicate %1 %2",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "colour": 355,
    "tooltip": "",
    "helpUrl": ","
  }
]);

Blockly.JavaScript['play_sound'] = function(block) {
  let value = '\'' + block.getFieldValue('VALUE') + '\'';
  return 'MusicMaker.queueSound(' + value + ');\n';
};
